![Image](https://www.liquidplanner.com/wp-content/uploads/2019/04/project-methodology.jpg)
# **SCALING**

## **Table of Contents**
1. About
2. Types of Scaling
    * 2.1 Horizontal Scaling 
    * 2.2 Vertical Scaling 
3. Elements of a project 
4. Load Balancing
5. Documentaion / References



## 1. **About**
Scaling is the process of increasing or decreasing the capacity of the system by changing the number of processes available to service requests. Scaling out a system provides additional capacity, while scaling in a system reduces capacity. Scaling is also a critical part of configuring a deployment for high availability. 

Scaling projects serves as a great model for organizational growth when it comes to inputs, outputs, process, teams, and more. 
In software engineering, scalability is a desirable property of a system, a network, or a process, which indicates its ability to either handle growing amounts of work in a graceful manner or to be enlarged. 


## 2. **Types of Scaling** 
### **2.1 Horizontal Scaling :**

To scale horizontally (or scale out) means to add more nodes to a system, such as adding a new computer to a distributed software application. An example might be scaling out from one web server system to three. 

### **2.2 Vertical Scaling :**

“Scale up” is when you upgrade a machine to a more powerful machine (e.g. faster CPU, faster GPU, engine with more HP, etc…) to get more processing power. 

```
 To scale vertically (or scale up) means to add : 
 1. resources (addition of CPUs or memory) or  
 2. process (addition of the same process) to a single node in a system.
```

Such vertical scaling of existing systems also enables them to use virtualization technology more effectively, as it provides more resources for the hosted set of operating system and application modules to share.


## 3. **Elements of a project :**

### **3.1 Budgets :**
Every project has a budget that needs to be tracked to by the project manager. The budget usually accompanies an invoice schedule tied to dates or milestones. As projects grow, budgets get more complex, and often estimation methods need to change.

### **3.2 Scope :**
Each project has definition of scope that may or may not change, and often becomes more clear throughout the course of a project.
As projects grow, uncertainty towards the beginning of projects tends to increase, making scope more difficult to define early.

### **3.3 Risk Management :**
On the whole, project managers need to manage risk of all forms for the projects.
With growing projects comes increasing complexity, and the PM role focuses more heavily on risk management and facilitating great communication.

### **3.4 Performance :**
Project performance measurement isn’t just on-time and on-budget.When we think about project performance measurement, it’s not really the same as measuring the performance of a team or a business process or an organisation’s strategy. 

When we measure the performance of the business process or team, we’re interested in how a particular business result produced by that process or team is changing as time goes by. 
- [7 Essential Project Performance Measure](https://www.staceybarr.com/measure-up/7-essential-project-performance-measures/)

## 4. **Load Balancing :**
Load balancing refers to efficiently distributing incoming network traffic across a group of backend servers, also known as a server farm or server pool.

a load balancer performs the following functions :
```
 1.Distributes client requests or network load efficiently across multiple servers.
 2.Ensures high availability and reliability by sending requests only to servers that are online.
 3.Provides the flexibility to add or subtract servers as demand dictates.
```

load balancing diagram

![Image](https://www.nginx.com/wp-content/uploads/2014/07/what-is-load-balancing-diagram-NGINX-640x324.png)



## 5. **Documentation / Reference :**
- [Scalability](https://en.wikipedia.org/wiki/Scalability)
- [Network Scalabilty](https://en.wikipedia.org/wiki/Scalability#Network_scalability)
- [Database Scalability](https://en.wikipedia.org/wiki/Scalability#Database_scalability)
- [Load Balancing](https://www.nginx.com/resources/glossary/load-balancing/)
- [What is  Load Balancing?](https://youtu.be/17AFseaBDgk)on YouTube

